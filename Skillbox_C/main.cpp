#include <iostream>


using namespace std;

class Animal
{
public:
    virtual void Voice() const {cout << "Roar!" << endl;}
};

class Dog : public Animal
{
public:
    void Voice() const override {cout << "Woof!" << endl;}
};

class Cat : public Animal
{
public:
    void Voice() const override {cout << "Purr!" << endl;}
};

class Cow : public Animal
{
public:
    void Voice() const override {cout << "Muuu!" << endl;}
};

int main()
{
    Animal *Zoo[3];
    Zoo[0] = new Dog;
    Zoo[1] = new Cat;
    Zoo[2] = new Cow;
    
    
    for (int i = 0 ; i < 3 ; i++)
    {
        Animal *ThisAnimal = Zoo[i];
        ThisAnimal->Voice();
    }
}
